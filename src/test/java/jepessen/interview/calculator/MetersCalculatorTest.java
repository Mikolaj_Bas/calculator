package jepessen.interview.calculator;

import jepessen.interview.calculator.calc.Calculator;
import jepessen.interview.calculator.calc.MetersCalculator;
import jepessen.interview.calculator.calc.MetersCalculator;
import jepessen.interview.calculator.data.FeetLength;
import jepessen.interview.calculator.data.MetersLength;
import jepessen.interview.calculator.data.Operation;
import jepessen.interview.calculator.data.SeaMilesLength;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MetersCalculatorTest {

    @Test
    public void testAdd() {

        Calculator calculator = new MetersCalculator();
        FeetLength second = FeetLength.from(1);
        MetersLength first = MetersLength.from(2);
        Operation operation = Operation.ADD;
        assertEquals(2.3048, calculator.calculate(first, second, operation).getValue(), 1e-4);
    }

    @Test
    public void testSubstract() {

        Calculator calculator = new MetersCalculator();
        MetersLength first = MetersLength.from(2);
        FeetLength second = FeetLength.from(1);
        Operation operation = Operation.SUBSTRACT;
        assertEquals(1.6952, calculator.calculate(first, second, operation).getValue(), 1e-4);
    }

    @Test
    public void testMultiply() {

        Calculator calculator = new MetersCalculator();
        FeetLength first = FeetLength.from(1);
        MetersLength second = MetersLength.from(1);
        Operation operation = Operation.MULTIPLY;
        assertEquals(0.3048, calculator.calculate(first, second, operation).getValue(), 1e-4);
    }

    @Test
    public void testDivide() {

        Calculator calculator = new MetersCalculator();
        FeetLength first = FeetLength.from(1);
        MetersLength second = MetersLength.from(1);
        Operation operation = Operation.DIVIDE;
        assertEquals(0.3048, calculator.calculate(first, second, operation).getValue(), 1e-4);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testDivideWithZero() {

        Calculator calculator = new MetersCalculator();
        FeetLength first = FeetLength.from(2);
        SeaMilesLength second = SeaMilesLength.from(0);
        Operation operation = Operation.DIVIDE;
        calculator.calculate(first, second, operation);
    }
}
