package jepessen.interview.calculator;

import jepessen.interview.calculator.data.FeetLength;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FeetLengthTest {

    @Test
    public void testToString() {
        assertEquals("22.0 ft", FeetLength.from(22).toString());
    }

    @Test
    public void testToMeters() {
        assertEquals(0.3048, FeetLength.from(1).toMeters().getValue(), 1e-4);
    }
}
