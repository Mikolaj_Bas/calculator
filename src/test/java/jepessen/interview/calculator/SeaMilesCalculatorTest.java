package jepessen.interview.calculator;

import jepessen.interview.calculator.calc.Calculator;
import jepessen.interview.calculator.calc.SeaMilesCalculator;
import jepessen.interview.calculator.data.MetersLength;
import jepessen.interview.calculator.data.Operation;
import jepessen.interview.calculator.data.SeaMilesLength;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SeaMilesCalculatorTest {

    @Test
    public void testAdd() {

        Calculator calculator = new SeaMilesCalculator();
        SeaMilesLength second = SeaMilesLength.from(1);
        MetersLength first = MetersLength.from(2);
        Operation operation = Operation.ADD;
        assertEquals(1853.852, calculator.calculate(first, second, operation).getValue(), 1e-4);
    }

    @Test
    public void testSubstract() {

        Calculator calculator = new SeaMilesCalculator();
        MetersLength first = MetersLength.from(1);
        SeaMilesLength second = SeaMilesLength.from(1);
        Operation operation = Operation.SUBSTRACT;
        assertEquals(1850.852, calculator.calculate(first, second, operation).getValue(), 1e-4);
    }

    @Test
    public void testMultiply() {

        Calculator calculator = new SeaMilesCalculator();
        SeaMilesLength first = SeaMilesLength.from(1);
        MetersLength second = MetersLength.from(10);
        Operation operation = Operation.MULTIPLY;
        assertEquals(18538.52, calculator.calculate(first, second, operation).getValue(), 1e-4);
    }

    @Test
    public void testDivide() {

        Calculator calculator = new SeaMilesCalculator();
        SeaMilesLength first = SeaMilesLength.from(1);
        MetersLength second = MetersLength.from(10);
        Operation operation = Operation.DIVIDE;
        assertEquals(185.3852, calculator.calculate(first, second, operation).getValue(), 1e-4);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testDivideWithZero() {

        Calculator calculator = new SeaMilesCalculator();
        SeaMilesLength first = SeaMilesLength.from(2);
        SeaMilesLength second = SeaMilesLength.from(0);
        Operation operation = Operation.DIVIDE;
        calculator.calculate(first, second, operation);
    }
}
