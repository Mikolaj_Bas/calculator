package jepessen.interview.calculator;

import jepessen.interview.calculator.data.FeetLength;
import jepessen.interview.calculator.data.MetersLength;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MetersLengthTest {

    @Test
    public void testToString() {
        assertEquals("11.0 m", MetersLength.from(11).toString());
    }

    @Test
    public void testToFeet() {
        assertEquals( 3.2808 , MetersLength.from(1).toFeet().getValue(), 1e-4);
    }
}
