package jepessen.interview.calculator;

import jepessen.interview.calculator.calc.Calculator;
import jepessen.interview.calculator.calc.SeaMilesCalculator;
import jepessen.interview.calculator.calc.SeaMilesCalculator;
import jepessen.interview.calculator.data.SeaMilesLength;
import jepessen.interview.calculator.data.MetersLength;
import jepessen.interview.calculator.data.Operation;
import jepessen.interview.calculator.data.SeaMilesLength;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SeaMilesLengthTest {

    @Test
    public void testToString() {
        assertEquals("11.0 Mm", SeaMilesLength.from(11).toString());
    }

    @Test
    public void testToFeet() {
        assertEquals( 1851.852 , SeaMilesLength.from(1).toMeters().getValue(), 1e-4);
    }
}
