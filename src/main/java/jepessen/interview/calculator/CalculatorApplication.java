package jepessen.interview.calculator;

import jepessen.interview.calculator.io.CommandLineIOInterface;
import jepessen.interview.calculator.io.InputData;
import jepessen.interview.calculator.io.WebIOInterface;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.Scanner;

@SpringBootApplication
public class CalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculatorApplication.class, args);
		Scanner scanner = new Scanner(System.in);
		CommandLineIOInterface commandLineIOInterface = new CommandLineIOInterface(scanner);
		while (true) {
			System.out.println("Give first value: ");
			String first = scanner.nextLine();
			System.out.println("Give second value: ");
			String second = scanner.nextLine();
			System.out.println("Give operation type: ");
			String operation = scanner.nextLine();
			System.out.println("Give result type: ");
			String resultType = scanner.nextLine();
			System.out.println(commandLineIOInterface.calculate(
					new InputData(first, second, resultType, operation)));
		}
	}
}
