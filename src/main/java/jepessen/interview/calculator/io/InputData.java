package jepessen.interview.calculator.io;

public class InputData {
    String first;
    String second;
    String resultType;
    String operation;

    public InputData(String first, String second, String resultType, String operation) {
        this.first = first;
        this.second = second;
        this.resultType = resultType;
        this.operation = operation;
    }

    public InputData() {}
}
