package jepessen.interview.calculator.io;

public interface InputOutputInterface {
    Object calculate(InputData inputData);
}
