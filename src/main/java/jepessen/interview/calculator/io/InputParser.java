package jepessen.interview.calculator.io;

import jepessen.interview.calculator.calc.Calculator;
import jepessen.interview.calculator.calc.FeetCalculator;
import jepessen.interview.calculator.calc.MetersCalculator;
import jepessen.interview.calculator.calc.SeaMilesCalculator;
import jepessen.interview.calculator.data.*;

public final class InputParser {

    public static Length parse(String lengthString) {
        if(lengthString == null || lengthString.trim().length() < 1) {
            throw new IllegalArgumentException("Given string cannot be empty");
        } else {
            String[] parts = lengthString.split(" ");
            if(parts.length != 2) {
                throw new IllegalArgumentException("Given data should contain length value and unit");
            }
            double value = parseValue(parts[0].trim());
            return parseUnit(parts[1].trim(), value);
        }
    }

    public static Operation parseOperation(String operationString) {
        switch (operationString) {
            case "+":
                return Operation.ADD;
            case "-":
                return Operation.SUBSTRACT;
            case "*":
                return Operation.MULTIPLY;
            case "/":
                return Operation.DIVIDE;
            default:
                throw new IllegalArgumentException("Choosen operation must be one from the list: '+', '*', '-', '/'");
        }
    }

    public static Calculator chooseCalculator(String resultType) {
        Calculator calculator;
        switch(resultType) {
            case "m":
                calculator = new MetersCalculator();
                break;
            case  "Mm":
                calculator = new SeaMilesCalculator();
                break;
            case "ft":
                calculator = new FeetCalculator();
                break;
            default:
                throw new IllegalArgumentException("Result unit type must be a value from the list: 'm', 'Mm', 'ft'");
        }
        return calculator;
    }

    private static Length parseUnit(String unitString, double value) {
        Length result;
        switch(unitString) {
            case "m":
                result = MetersLength.from(value);
                break;
            case  "Mm":
                result = SeaMilesLength.from(value);
                break;
            case "ft":
                result = FeetLength.from(value);
                break;
            default:
                throw new IllegalArgumentException("Length unit must be a value from the list: 'm', 'Mm', 'ft'");
        }
        return result;
    }

    private static double parseValue(String valueString) {
        double value;
        try {
            value = Integer.parseInt(valueString);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Length value must be a number");
        }
        return value;
    }
}
