package jepessen.interview.calculator.io;

import jepessen.interview.calculator.calc.Calculator;
import jepessen.interview.calculator.data.Length;
import jepessen.interview.calculator.data.Operation;

import java.util.Scanner;

public class CommandLineIOInterface implements InputOutputInterface {

    private Scanner scanner;

    public CommandLineIOInterface(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public String calculate(InputData inputData) {
        Length firstLength;
        Length secondLength;
        try {
            firstLength = InputParser.parse(inputData.first);
            secondLength = InputParser.parse(inputData.second);
            Operation operation = InputParser.parseOperation(inputData.operation);
            Calculator calculator = InputParser.chooseCalculator(inputData.resultType);
            return calculator.calculate(firstLength, secondLength, operation).toString();
        } catch (IllegalArgumentException ex) {
            return ex.getMessage();
        } catch (UnsupportedOperationException ex) {
            return ex.getMessage();
        }
    }
}
