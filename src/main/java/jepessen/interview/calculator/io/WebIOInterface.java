package jepessen.interview.calculator.io;


import jepessen.interview.calculator.calc.Calculator;
import jepessen.interview.calculator.data.Length;
import jepessen.interview.calculator.data.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class WebIOInterface implements InputOutputInterface {

    @Override
    @ResponseBody
    @RequestMapping("/calculate")
    public ResponseEntity<String> calculate(@RequestBody InputData input) {
        try {
            Length firstLength = InputParser.parse(input.first);
            Length secondLength = InputParser.parse(input.second);
            Operation operation = InputParser.parseOperation(input.operation);
            Calculator calculator = InputParser.chooseCalculator(input.resultType);
            return ResponseEntity.ok().body(calculator.calculate(firstLength, secondLength, operation).toString());
        } catch (IllegalArgumentException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        } catch (UnsupportedOperationException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }
}
