package jepessen.interview.calculator.data;

public interface Length {

    double FEET_MEETER_MODIFIER = 0.3048;

    double SEA_MILES_METER_MODIFIER = 1851.852;

    MetersLength toMeters();

    SeaMilesLength toSeaMiles();

    FeetLength toFeet();

    double getValue();
}
