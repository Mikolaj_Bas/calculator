package jepessen.interview.calculator.data;

public enum Operation {
    ADD("+"),
    SUBSTRACT("-"),
    MULTIPLY("*"),
    DIVIDE("/");

    private final String value;

    Operation(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }
}
