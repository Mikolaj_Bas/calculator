package jepessen.interview.calculator.data;

public class MetersLength implements Length {

    private double value;

    private MetersLength(double value) {
        this.value = value;
    }

    public static MetersLength from(double value) {
        return new MetersLength(value);
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public MetersLength toMeters() {
        return this;
    }

    @Override
    public SeaMilesLength toSeaMiles() {
        return SeaMilesLength.from((1 / SEA_MILES_METER_MODIFIER) * this.value);
    }

    @Override
    public FeetLength toFeet() {
        return FeetLength.from((1 / FEET_MEETER_MODIFIER) * this.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MetersLength that = (MetersLength) o;

        return Double.compare(that.value, value) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(value);
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        return value + " m";
    }
}
