package jepessen.interview.calculator.data;

public class FeetLength implements Length {

    private double value;

    private FeetLength(double value) {
        this.value = value;
    }

    public static FeetLength from(double value) {
        return new FeetLength(value);
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public MetersLength toMeters() {
        return MetersLength.from(FEET_MEETER_MODIFIER * value);
    }

    @Override
    public SeaMilesLength toSeaMiles() {
        return SeaMilesLength.from(SEA_MILES_METER_MODIFIER * (1 / FEET_MEETER_MODIFIER) * value);
    }

    @Override
    public FeetLength toFeet() {
        return this;
    }

    @Override
    public String toString() {
        return value + " ft";
    }
}
