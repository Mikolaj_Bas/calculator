package jepessen.interview.calculator.data;

public class SeaMilesLength implements Length {

    private double value;

    private SeaMilesLength(double value){
        this.value = value;
    }

    public static SeaMilesLength from(double value) {
        return new SeaMilesLength(value);
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public MetersLength toMeters() {
        return MetersLength.from(SEA_MILES_METER_MODIFIER * this.value);
    }

    @Override
    public SeaMilesLength toSeaMiles() {
        return this;
    }

    @Override
    public FeetLength toFeet() {
        return FeetLength.from(SEA_MILES_METER_MODIFIER * (1 / FEET_MEETER_MODIFIER) * this.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SeaMilesLength that = (SeaMilesLength) o;

        return Double.compare(that.value, value) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(value);
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        return value + " Mm";
    }
}
