package jepessen.interview.calculator.calc;

import jepessen.interview.calculator.data.FeetLength;
import jepessen.interview.calculator.data.Length;
import jepessen.interview.calculator.data.MetersLength;
import jepessen.interview.calculator.data.Operation;

public class MetersCalculator extends Calculator {

    @Override
    public Length calculate(Length first, Length second, Operation operation) {
        return MetersLength.from(chooseMethod(first.toMeters(), second.toMeters(), operation));
    }
}
