package jepessen.interview.calculator.calc;

import jepessen.interview.calculator.data.FeetLength;
import jepessen.interview.calculator.data.Length;
import jepessen.interview.calculator.data.Operation;

public class FeetCalculator extends Calculator {

    @Override
    public Length calculate(Length first, Length second, Operation operation) {
        return FeetLength.from(chooseMethod(first.toFeet(), second.toFeet(), operation));
    }
}
