package jepessen.interview.calculator.calc;

import jepessen.interview.calculator.data.*;
import jepessen.interview.calculator.data.SeaMilesLength;

public class SeaMilesCalculator extends Calculator {

    @Override
    public Length calculate(Length first, Length second, Operation operation) {
        return SeaMilesLength.from(chooseMethod(first.toSeaMiles(), second.toSeaMiles(), operation));
    }
}
