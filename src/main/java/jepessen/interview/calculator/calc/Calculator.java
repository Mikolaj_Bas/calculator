package jepessen.interview.calculator.calc;

import jepessen.interview.calculator.data.Length;
import jepessen.interview.calculator.data.Operation;

public abstract class Calculator {

    public abstract Length calculate(Length first, Length second, Operation operation);

    protected double chooseMethod(Length first, Length second, Operation operation) {
        switch (operation) {
            case ADD:
                return add(first, second);
            case SUBSTRACT:
                return substract(first, second);
            case MULTIPLY:
                return multiply(first, second);
            default:
                return divide(first, second);
        }
    }

    private double add(Length first, Length second) {
        return first.getValue() + second.getValue();
    }

    private double substract(Length first, Length second) {
        return first.getValue() - second.getValue();
    }

    private double multiply(Length first, Length second) {
        return first.getValue() * second.getValue();
    }

    private double divide(Length first, Length second) {
        if(second.getValue() == 0) {
            throw new UnsupportedOperationException("Cannot divide by zero");
        }
        return first.getValue() / second.getValue();
    }

}
